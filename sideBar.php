<?php

?>
<!-- Sidebar-->
<div class="border-end bg-white" id="sidebar-wrapper">
    <div class="sidebar-heading border-bottom bg-light">Menú Sistema</div>
    <div class="list-group list-group-flush">
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="MaestroProductos.php">Mantenedor Productos</a>
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="MaestroClientes.php">Mantenedor Clientes</a>
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="MaestroProveedores.php">Mantenedor Proveedores</a>
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="MenuTransacciones.php">Transacciones</a>
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="ResumenVentas.php">Resumen Compras</a>
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="Stock.php">Actualizar Stock</a>
        <a class="list-group-item list-group-item-action list-group-item-light p-3" href="#!">Status</a>
    </div>
</div>

<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>