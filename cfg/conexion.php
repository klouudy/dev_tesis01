<?php
Class mysqlClass{

    private $servidor='localhost';
    private $usuario='root';
    private $password='';
    private $base_datos='bd_tesis';
    private $link;
 
    static $_instance;


    
    //Realiza la conexión a la base de datos.
    public function conectar(){
      $this->link=new mysqli($this->servidor, $this->usuario, $this->password,$this->base_datos,$this->link);
      $this->link->set_charset("utf8");
      return $this->link;
   }

}
?>