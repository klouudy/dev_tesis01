<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>
            <?php echo $tittleApp; ?>
        </title>
        <meta name="description" content="RowGroup">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        
        <meta name="apple-mobile-web-app-capable" content="yes" />

        <meta name="msapplication-tap-highlight" content="no">

        <link rel="stylesheet" media="screen, print" href="css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="css/app.bundle.css">
        <link rel="stylesheet" media="screen, print" href="css/uploadFile.css">
        <link rel="stylesheet" media="screen, print" href="css/general.css">
        <link rel="stylesheet" media="screen, print" href="css/select2.min.css">
        
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="stylesheet" media="screen, print" href="css/datagrid/datatables/datatables.bundle.css">
        <script src="js/decimal.min.js" ></script>
    <style>
        .nav-link.active{
        background-color: rgb(99,139,25) !important;
        border-bottom: 1px solid rgb(99,139,25) !important;
        color: white !important;
        }
    </style>
    </head>
    