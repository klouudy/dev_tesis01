<?php 
    include 'cfg/variablesFijas.php';
    include 'modal/modal_respuesta.html';

?>


<!DOCTYPE html>
<html lang="es">
  <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <title>
          <?php echo $tittleApp; ?>
      </title>

      <!-- Favicon-->
      <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
      <!-- Core theme CSS (includes Bootstrap)-->
      <link href="css/styles.css" rel="stylesheet" />
      <link rel="stylesheet" media="screen, print" href="css/datagrid/datatables/datatables.bundle.css">

  </head>
  <body>
      <div class="d-flex" id="wrapper">
          <?php
            include 'sideBar.php';
          ?>
          <!-- Page content wrapper-->
          <div id="page-content-wrapper">
              <?php
                include 'topBar.php';
              ?>
              <!-- Page content-->
              <div class="container-fluid">
                  <h1 class="mt-4">Menu principal</h1>
              </div>
          </div>
      </div>
      <!-- Bootstrap core JS-->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
      <!-- Core theme JS-->
      <script src="js/scripts.js"></script>
  </body>
</html>

