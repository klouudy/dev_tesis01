<?php include 'cfg/variablesFijas.php' ?>
<?php include 'modal/modal_respuesta.html'; ?>
<?php include 'modal/modal_alerta.html'; ?>
<?php
?>

<!DOCTYPE html>
<html lang="es">

    <head>
    <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <title>
          <?php echo $tittleApp; ?>
      </title>

      <!-- Favicon-->
      <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
      <!-- Core theme CSS (includes Bootstrap)-->
      <link href="css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
        

    </head>
<body>
    <!-- <div class="page-wrapper"> -->
    <div class="d-flex" id="wrapper">
          <?php
            include 'sideBar.php';
          ?>
          <!-- Page content wrapper-->
          <div id="page-content-wrapper">
              <?php
                include 'topBar.php';
              ?>

                <!-- MAIN PAGE -->
                <main id="js-page-content" role="main" class="page-content">
                    
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="container">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ol class="breadcrumb page-breadcrumb">
                                                <li class="breadcrumb-item active">Maestro Proveedores</li>
                                            </ol>
                                            <div class="subheader">
                                                <h1 class="subheader-title">
                                                    <i class="subheader-icon fal fa-pencil"></i> Maestro Proveedores
                                                </h1>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" action="">
                                                <table id="tbProveedor" class=" table table-sm table-bordered table-hover table-striped w-100">
                                                    <thead class="bg-highlight">
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Nombre</th>
                                                            <th>Direccion</th>
                                                            <th>Ciudad</th>
                                                            <th>Telefono</th>
                                                            <th>Contacto</th>
                                                        </tr>
                                                    </thead>
                                                    <tbod>
                                                    </tbody>
                                                    <tfoot class="thead-themed" >
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>Nombre</th>
                                                            <th>Direccion</th>
                                                            <th>Ciudad</th>
                                                            <th>Telefono</th>
                                                            <th>Contacto</th>
                                                        </tr>
                                                    </tfoot>
                                                </table><br>
                                                
                                            </form>
                                        </div>
                                    </div>                                    
                                </div> 
                            </div>
                        </div>
                    </div>       
                    <div id = "modal"></div>
                </main>
                
                <!-- BEGIN Page Footer -->

                <footer class="page-footer" role="contentinfo">
                    <div class="d-flex align-items-center flex-1 text-muted">
                        <span class="hidden-md-down fw-700">Negocio Tia del Brayan</span>
                    </div>
                </footer>
                <!-- END Page Footer -->
                
            </div>
        </div>
    <!-- </div> -->
    
    
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    
    <!-- <script src="js/datagrid/datatables/datatables.bundle.js"></script> -->
    
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>

    
    <script src="ajax/fn_maestroproveedores.js"></script>
</body>
</html>