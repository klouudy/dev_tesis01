<?php
    
    require_once "cfg/conexion.php";
    date_default_timezone_set('America/Santiago');

    function SPrecordset_SPWEB_Save_Clientes($datos){
        
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        
        $sql = $conn->prepare("CALL SPWEB_Save_Clientes(?,?,?,?,?,?,?,?,?,?);"); 
        $sql->bind_param("isssssssis"
                        ,$datos["data"]["id"]
                        ,$datos["data"]["rut"]
                        ,$datos["data"]["dv"]
                        ,$datos["data"]["nombre"]
                        ,$datos["data"]["direccion"]
                        ,$datos["data"]["ciudad"]
                        ,$datos["data"]["telefono"]
                        ,$datos["data"]["contacto"]
                        ,$out_resultado
                        ,$out_respuesta);
        $result = $sql->execute();
        $sql->bind_result($out_resultado,$out_respuesta);

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 138";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
            $arr    ["data"]["resultado"]   =   "1";
            $arr    ["data"]["respuesta"]   =   "Ha ocurrido un error, no se ha registrado la informacion de manera correcta."; 
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";

            while ($sql->fetch()) {
                $arr    ["data"]["resultado"]   =   $out_resultado;
                $arr    ["data"]["respuesta"]   =   $out_respuesta;
            }           
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Search_Clientes($datos){
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        $sql = 'CALL SPWEB_Search_Clientes()';
        $result = $conn->query($sql);   

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 135";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
        }else{             
            $arr    ["data"]   =    array();
            $i = 0; 
            while ($row =  $result->fetch_assoc()){
                $arr ["data"][$i] = array(); 
                while(list($var, $val) = each($row)) {
                    $arr ["data"][$i][$var] =   ($val);
                }                
                $i++; 
            }                       
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Delete_Clientes($datos){
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar(); 
        $sql = $conn->prepare("CALL SPWEB_Delete_Clientes(?,?,?);");
         
        $sql->bind_param("iis",
        $datos["data"]["id"],
        $out_resultado,
        $out_respuesta);
        $result = $sql->execute();
        $sql->bind_result($out_resultado,$out_respuesta);

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 203";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
            $arr    ["data"]["resultado"]   =   "1";
            $arr    ["data"]["respuesta"]   =   "Ha ocurrido un error, no se ha registrado la informacion de manera correcta."; 
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";

            while ($sql->fetch()) {
                $arr    ["data"]["resultado"]   =   $out_resultado;
                $arr    ["data"]["respuesta"]   =   $out_respuesta;
            }           
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Save_Producto($datos){
        
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        
        $sql = $conn->prepare("CALL SPWEB_Save_Producto(?,?,?,?,?,?,?,?);"); 
        $sql->bind_param("isssssssis"
                        ,$datos["data"]["id"]
                        ,$datos["data"]["descripcion"]
                        ,$datos["data"]["uni_med"]
                        ,$datos["data"]["proveedor"]
                        ,$datos["data"]["stock_min"]
                        ,$datos["data"]["tipo_prod"]
                        ,$out_resultado
                        ,$out_respuesta);
        $result = $sql->execute();
        $sql->bind_result($out_resultado,$out_respuesta);

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 138";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
            $arr    ["data"]["resultado"]   =   "1";
            $arr    ["data"]["respuesta"]   =   "Ha ocurrido un error, no se ha registrado la informacion de manera correcta."; 
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";

            while ($sql->fetch()) {
                $arr    ["data"]["resultado"]   =   $out_resultado;
                $arr    ["data"]["respuesta"]   =   $out_respuesta;
            }           
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Search_Producto($datos){
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        $sql = 'CALL SPWEB_Search_Producto()';
        $result = $conn->query($sql);   

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 135";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
        }else{             
            $arr    ["data"]   =    array();
            $i = 0; 
            while ($row =  $result->fetch_assoc()){
                $arr ["data"][$i] = array(); 
                while(list($var, $val) = each($row)) {
                    $arr ["data"][$i][$var] =   ($val);
                }                
                $i++; 
            }                       
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Save_Tipo_Producto($datos){
        
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        
        $sql = $conn->prepare("CALL SPWEB_Save_Tipo_Producto(?,?,?,?,?,?,?,?,?,?);"); 
        $sql->bind_param("isssssssis"
                        ,$datos["data"]["id"]
                        ,$datos["data"]["rut"]
                        ,$datos["data"]["dv"]
                        ,$datos["data"]["nombre"]
                        ,$datos["data"]["direccion"]
                        ,$datos["data"]["ciudad"]
                        ,$datos["data"]["telefono"]
                        ,$datos["data"]["contacto"]
                        ,$out_resultado
                        ,$out_respuesta);
        $result = $sql->execute();
        $sql->bind_result($out_resultado,$out_respuesta);

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 138";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
            $arr    ["data"]["resultado"]   =   "1";
            $arr    ["data"]["respuesta"]   =   "Ha ocurrido un error, no se ha registrado la informacion de manera correcta."; 
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";

            while ($sql->fetch()) {
                $arr    ["data"]["resultado"]   =   $out_resultado;
                $arr    ["data"]["respuesta"]   =   $out_respuesta;
            }           
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Search_Tipo_Producto($datos){
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        $sql = 'CALL SPWEB_Search_Tipo_Producto()';
        $result = $conn->query($sql);   

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 135";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
        }else{             
            $arr    ["data"]   =    array();
            $i = 0; 
            while ($row =  $result->fetch_assoc()){
                $arr ["data"][$i] = array(); 
                while(list($var, $val) = each($row)) {
                    $arr ["data"][$i][$var] =   ($val);
                }                
                $i++; 
            }                       
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Save_Proveedor($datos){
        
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        
        $sql = $conn->prepare("CALL SPWEB_Save_Proveedor(?,?,?,?,?,?,?,?);"); 
        $sql->bind_param("isssssssis"
                        ,$datos["data"]["id"]
                        ,$datos["data"]["nombre"]
                        ,$datos["data"]["direccion"]
                        ,$datos["data"]["ciudad"]
                        ,$datos["data"]["fono"]
                        ,$datos["data"]["contacto"]
                        ,$out_resultado
                        ,$out_respuesta);
        $result = $sql->execute();
        $sql->bind_result($out_resultado,$out_respuesta);

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 138";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
            $arr    ["data"]["resultado"]   =   "1";
            $arr    ["data"]["respuesta"]   =   "Ha ocurrido un error, no se ha registrado la informacion de manera correcta."; 
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";

            while ($sql->fetch()) {
                $arr    ["data"]["resultado"]   =   $out_resultado;
                $arr    ["data"]["respuesta"]   =   $out_respuesta;
            }           
        }
        
        $conn->close();
        return $arr;
    }

    function SPrecordset_SPWEB_Search_Proveedor($datos){
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        $sql = 'CALL SPWEB_Search_Proveedor()';
        $result = $conn->query($sql);   

        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 135";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
        }else{             
            $arr    ["data"]   =    array();
            $i = 0; 
            while ($row =  $result->fetch_assoc()){
                $arr ["data"][$i] = array(); 
                while(list($var, $val) = each($row)) {
                    $arr ["data"][$i][$var] =   ($val);
                }                
                $i++; 
            }                       
        }
        
        $conn->close();
        return $arr;
    }
    function SPrecordset_SPWEB_Search_CMB($datos){
        $arr    =   array();
        $cmb    =   $datos["cmb"];
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        $sql = $datos["sp"];

        $result = $conn->query($sql);
        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 195";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
            $arr    ["data"]["listado"] =   array();
            $arr    ["data"]["cmb"]     =   $cmb;
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";
            $arr    ["data"]["listado"]     =   array();
            $arr    ["data"]["cmb"]         =   $cmb;
            $i = 0;
            while ($row =  $result->fetch_assoc()){
                $arr ["data"]["listado"][$i] = array();
                while(list($var, $val) = each($row)) {
                    $arr ["data"]["listado"][$i][$var]  =   ($val);
                }                
                $i++;
            }         
        }
        
        $conn->close();
        return $arr;
    }
    function SPrecordset_SPWEB_Search_Documento($datos){
        $arr    =   array();
        $db     = new mysqlClass();
        $conn   = $db->conectar();
        
        $sql = $conn->prepare("CALL SPWEB_Search_Documento(?,?,?,?);");
        $sql->bind_param("iiss"
                        ,$datos["cliente"]
                        ,$datos["tipoDocumento"]
                        ,$datos["fechaDesde"]
                        ,$datos["fechaHasta"]);
        $result = $sql->execute();
        
        if($result === false){
            $arr    ["success"]         =   false;
            $arr    ["data"]["mensaje"] =   "Error al ejecutar llamado 211";
            $arr    ["data"]["errors"]  =   array(mysqli_error($conn));
        }else{            
            $arr    ["success"]             =   true;
            $arr    ["data"]["mensaje"]     =   "Existe informacion para desplegar";
            $arr    ["data"]["listado"]     =   array();
            $i = 0;
            $result = $sql->get_result();
        
            while ($row =  $result->fetch_array(MYSQLI_ASSOC)){
                $arr ["data"]["listado"][$i] = array();
                while(list($var, $val) = each($row)) {
                    $arr ["data"]["listado"][$i][$var]  = ($val);
                }                
                $i++;
            }                         
        }
        
        $conn->close();
        return $arr;
    }

    ?>