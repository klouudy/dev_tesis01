<?php include 'cfg/variablesFijas.php' ?>
<?php include 'modal/modal_respuesta.html'; ?>
<?php include 'modal/modal_alerta.html'; ?>
<?php
?>

<!DOCTYPE html>
<html lang="es">

    <head>
    <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <title>
          <?php echo $tittleApp; ?>
      </title>

      <!-- Favicon-->
      <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
      <!-- Core theme CSS (includes Bootstrap)-->
      <link href="css/styles.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/jszip-2.5.0/dt-1.11.3/af-2.3.7/b-2.1.1/b-colvis-2.1.1/b-html5-2.1.1/b-print-2.1.1/cr-1.5.5/date-1.1.1/fc-4.0.1/fh-3.2.1/kt-2.6.4/r-2.2.9/rg-1.1.4/rr-1.2.8/sc-2.0.5/sb-1.3.0/sp-1.4.0/sl-1.3.4/sr-1.0.1/datatables.css"/>

    </head>
<body>
    <!-- <div class="page-wrapper"> -->
    <div class="d-flex" id="wrapper">
          <?php
            include 'sideBar.php';
          ?>
          <!-- Page content wrapper-->
          <div id="page-content-wrapper">
              <?php
                include 'topBar.php';
              ?>

                <!-- MAIN PAGE -->
                <main id="js-page-content" role="main" class="page-content">
                <div class="container">


  <div class="row">
    <div class="col-md-4 order-md-2 mb-4">
      <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted">Productos</span>
        <span class="badge badge-secondary badge-pill">3</span>
      </h4>
      <ul class="list-group mb-3">
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Product name</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">$12</span>
        </li>
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Second product</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">$8</span>
        </li>
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Third item</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">$5</span>
        </li>
        <li class="list-group-item d-flex justify-content-between bg-light">
          <div class="text-success">
            <h6 class="my-0">Promo code</h6>
            <small>EXAMPLECODE</small>
          </div>
          <span class="text-success">-$5</span>
        </li>
        <li class="list-group-item d-flex justify-content-between">
          <span>Total (USD)</span>
          <strong>$20</strong>
        </li>
      </ul>

      <form class="card p-2">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Promo code">
          <div class="input-group-append">
            <button type="submit" class="btn btn-secondary">Redeem</button>
          </div>
        </div>
      </form>
    </div>
    <br>
    <div class="col-md-8 order-md-1">
      <h4 class="mb-3">Detalle Documento</h4>
      <form class="needs-validation" novalidate>
        <div class="row">
          <div class="col-md-2 mb-3">
            <label for="firstName">ID</label>
            <input type="text" class="form-control" id="txtId" placeholder="" value="" required>
          </div>
          <div class="col-md-5 mb-3">
            <label for="firstName">CLIENTE/PROVEEDOR</label>
            <input type="text" class="form-control" id="firstName" placeholder="" value="" required>
          </div>
          <div class="col-md-5 mb-3">
            <label for="lastName">Tipo Documento</label>
            <input type="text" class="form-control" id="lastName" placeholder="" value="" required>
            <div class="invalid-feedback">
              Valid last name is required.
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4 mb-3">
            <label for="firstName">Fecha Emision</label>
            <input type="date" class="form-control" id="txtFecha" placeholder="" value="" required>
          </div>
          <div class="col-md-5 mb-3">
            <label for="firstName">Estado</label>
            <input type="text" class="form-control" id="txtEstado" placeholder="" value="" disabled>
          </div>
         
        </div>
        <div class="row">
          <div class="col-md-4 mb-3">
            <label for="firstName">Valor Neto</label>
            <input type="number" class="form-control" id="txtNeto" placeholder="" value="" required>
          </div>
          <div class="col-md-4 mb-3">
            <label for="firstName">Valor IVA</label>
            <input type="number" class="form-control" id="txtIva" placeholder="" value="" required>
          </div>
          <div class="col-md-4 mb-3">
            <label for="lastName">Valor Total</label>
            <input type="number" class="form-control" id="txtTotal" placeholder="" value="" required>
          </div>
        </div>
        <div class="row">
        <div class="col-md-10 mb-3">
            <label for="firstName">Comentarios</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
          </div>
        </div>
       
        <hr class="mb-2">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
      </form>
    </div>
  </div>      
                            
                </main>
                
                <!-- BEGIN Page Footer -->
                <br><br>
                <footer class="page-footer" role="contentinfo">
                    <div class="d-flex align-items-center flex-1 text-muted">
                        <span class="hidden-md-down fw-700">Negocio Tia del Brayan</span>
                    </div>
                </footer>
                <!-- END Page Footer -->
                
            </div>
        </div>
    <!-- </div> -->
    
    
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <!-- 
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script> -->
<!-- 
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/b-2.1.1/datatables.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script> -->

    
    <script src="js/jquery-1.3.min.js"></script>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/jszip-2.5.0/dt-1.11.3/af-2.3.7/b-2.1.1/b-colvis-2.1.1/b-html5-2.1.1/b-print-2.1.1/cr-1.5.5/date-1.1.1/fc-4.0.1/fh-3.2.1/kt-2.6.4/r-2.2.9/rg-1.1.4/rr-1.2.8/sc-2.0.5/sb-1.3.0/sp-1.4.0/sl-1.3.4/sr-1.0.1/datatables.js"></script>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script> -->
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <script src="ajax/fn_transacciones.js"></script>
    <script src="js/funciones.js"></script>
</body>
</html>