<?php include 'cfg/variablesFijas.php' ?>
<?php include 'modal/modal_respuesta.html'; ?>
<?php include 'modal/modal_alerta.html'; ?>
<?php
?>

<!DOCTYPE html>
<html lang="es">

    <head>
    <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="" />
      <meta name="author" content="" />
      <title>
          <?php echo $tittleApp; ?>
      </title>

      <!-- Favicon-->
      <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
      <!-- Core theme CSS (includes Bootstrap)-->
      <link href="css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/jszip-2.5.0/dt-1.11.3/af-2.3.7/b-2.1.1/b-colvis-2.1.1/b-html5-2.1.1/b-print-2.1.1/cr-1.5.5/date-1.1.1/fc-4.0.1/fh-3.2.1/kt-2.6.4/r-2.2.9/rg-1.1.4/rr-1.2.8/sc-2.0.5/sb-1.3.0/sp-1.4.0/sl-1.3.4/sr-1.0.1/datatables.css"/>

    </head>
<body>
    <!-- <div class="page-wrapper"> -->
    <div class="d-flex" id="wrapper">
          <?php
            include 'sideBar.php';
          ?>
          <!-- Page content wrapper-->
          <div id="page-content-wrapper">
              <?php
                include 'topBar.php';
              ?>

                <!-- MAIN PAGE -->
                <main id="js-page-content" role="main" class="page-content">
                    
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="container">
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ol class="breadcrumb page-breadcrumb">
                                                <li class="breadcrumb-item active">Maestro Productos</li>
                                            </ol>
                                            <div class="subheader">
                                                <h1 class="subheader-title">
                                                    <i class="subheader-icon fal fa-pencil"></i> Maestro Productos
                                                </h1>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <form method="post" action="">
                                                <div class="panel-content">
                                                    <table id="tbProducto" class="table" id="frmProducto" name="frmProducto">
                                                        
                                                        <thead class="bg-highlight">
                                                            <tr>
                                                                <th>ID</th>                                                            
                                                                <th>Descripcion</th>                                                            
                                                                <th>U. de Medida</th>                                                            
                                                                <th>Proveedor de Prod.</th>                                                            
                                                                <th>Stock de Prod.</th>                                                          
                                                                <th>Tipo Producto</th> 
                                                            </tr>
                                                        </thead>
                                                        <tbod>
                                                        </tbody>
                                                        <tfoot class="thead-themed" >
                                                            <tr>
                                                                <th>ID</th>                                                            
                                                                <th>Descripcion</th>                                                            
                                                                <th>Unidad de medida</th>                                                            
                                                                <th>Proveedor</th>                                                            
                                                                <th>Stock minimo</th>                                                           
                                                                <th>Tipo Producto</th>                                                           
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div id="modalProducto"></div>    
                                                </div>
                                            </form>
                                        </div>
                                        <div>
                                                <!--  -->
                                        </div>

                                        <div class="panel-body">
                                            <form method="post" action="">
                                                <table id="tbTipoProd" class="table">
                                                    <thead class="bg-highlight">
                                                        <tr>
                                                            <th>ID</th>                                                            
                                                            <th>Tipo Producto</th>      
                                                        </tr>
                                                    </thead>
                                                    <tbod>
                                                    </tbody>
                                                    <tfoot class="thead-themed" >
                                                        <tr>
                                                            <th>ID</th>                                                            
                                                            <th>Tipo Producto</th>                                                            
                                                        </tr>
                                                    </tfoot>
                                                </table><br>
                                                
                                            </form>
                                        </div>
                                    </div>                                    
                                </div> 
                            </div>
                        </div>
                    </div>       
                </main>
                
                <!-- BEGIN Page Footer -->

                <footer class="page-footer" role="contentinfo">
                    <div class="d-flex align-items-center flex-1 text-muted">
                        <span class="hidden-md-down fw-700">Negocio Tia del Brayan</span>
                    </div>
                </footer>
                <!-- END Page Footer -->
                
            </div>
        </div>
    <!-- </div> -->
    
    
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <!-- 
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script> -->
<!-- 
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/b-2.1.1/datatables.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script> -->

    
    <script src="js/jquery-1.3.min.js"></script>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/jszip-2.5.0/dt-1.11.3/af-2.3.7/b-2.1.1/b-colvis-2.1.1/b-html5-2.1.1/b-print-2.1.1/cr-1.5.5/date-1.1.1/fc-4.0.1/fh-3.2.1/kt-2.6.4/r-2.2.9/rg-1.1.4/rr-1.2.8/sc-2.0.5/sb-1.3.0/sp-1.4.0/sl-1.3.4/sr-1.0.1/datatables.js"></script>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script> -->
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <script src="ajax/fn_maestroproductos.js"></script>
    <script src="js/funciones.js"></script>
</body>
</html>