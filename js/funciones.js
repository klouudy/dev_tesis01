function fCargaCmb(arreglo,accion,funct,inic,def,disabl){	
	var cmb = $('#'+arreglo["data"]["cmb"]); //obtengo objeto
	cmb.find('option').remove();//limpiar cmb
	var cantidad 	=	arreglo['data']['listado'].length
		,codigo		=	-1;
	//Validar si se encontraron datos para combobox
	if (arreglo['data']['listado'].length > 0){
		if (inic != ''){	//se agrega valor inicial
			cmb.append('<option value="0">'+inic+'</option>');
		}
		$(arreglo['data']['listado']).each(function(i,v){	//llena cmb
			codigo	=	v.Codigo;
			cmb.append('<option value="'+v.Codigo+'">'+v.Descripcion+'</option>');			
		});

		if (accion != ''){
			cmb.attr(accion,funct);	//asigno accion / funcion
		}
		if (def!=''){	//asigna valor por defecto
			cmb.val(def);	
		}else if (cantidad == 1){
			cmb.val(codigo);
			cmb.change();
		}
		cmb.prop("disabled",disabl);	
	}else{
		cmb.append('<option value="-1">No se encontro informacion</option>');
	}
}