$(document).ready(function(){
    fBuscaProducto();
    fBuscaTipoProducto();
    modalProducto();

    let datosProveedor = {'busqueda': 'buscaProveedor', 'cmb': 'cmbProveedor', "accion": "", "fun" : "", "aux" : ""}
    fbuscaCmb(datosProveedor,'--Seleccionar--');
});

function fbuscaCmb(datos,optiondef,onchange){   
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                fCargaCmb(resultado,"onchange",onchange,optiondef,"",false);
                            }else{
                                console.log(resultado);
                            }
                        }
        ,error:function(response){ 
                        check_response_error(response);
                         console.log('error-->'+response);
                         console.log("Error. 4J4X-5C41PT01");
                        }
    });
}


function modalProducto(){



    let modalInf = '';
    modalInf += '<div class="modal fade" id="modalProducto1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
    modalInf += '<div class="modal-dialog modal-lg" role="document">';
    modalInf += '    <div class="modal-content">';
    modalInf += '        <div class="modal-header">';
    modalInf += '        <div class="modal-body">';
    modalInf += '            <div class="row">';
    modalInf += '                <div class="col-xl-12">';
    modalInf += '                    <div id="panel-1" class="panel">';
    modalInf += '                        <div class="panel-hdr">';
    modalInf += '                            <h2>';
    modalInf += '                               <span class="fw-300"><i>Ingreso Producto</i></span>';
    modalInf += '                            </h2>';
    modalInf += '                            <div class="panel-toolbar">';
    modalInf += '                                <button class="btn btn-panel waves-effect waves-themed" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>';
    modalInf += '                                <button class="btn btn-panel waves-effect waves-themed" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>';                   
    modalInf += '                            </div>';
    modalInf += '                        </div>';
    modalInf += '                        <div class="panel-container show">';
    modalInf += '                            <div class="panel-content">';
    modalInf += '                               <form method="post" action="" id="" name="">'; 

    modalInf += '                                           <input type="text" class="form-control" id="txtIdProducto" name="txtIdProducto" value="0" hidden>';

    modalInf += '                                   <div class="form-group row">';

    modalInf += '                                       <label class="form-label col-sm-2 col-form-label text-left text-sm-right" for="operacion">Descripcion:</label>';
    
    modalInf += '                                       <div class="col-lg-4">';
    modalInf += '                                           <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion">';
        
    modalInf += '                                       </div>';

    modalInf += '                                   </div>';

    modalInf += '                                   </br>';
    
    modalInf += '                                   <div class="form-group row">';

    modalInf += '                                       <label class="form-label col-sm-2 col-form-label text-left text-sm-right" for="operacion">Unidad de medida:</label>';
    
    modalInf += '                                       <div class="col-lg-4">';    
    modalInf += '                                           <input type="text" class="form-control" id="txtUnimed" name="txtUnimed">';
    modalInf += '                                       </div>';
    
    
    modalInf += '                                   </div>';
    
    modalInf += '                                   </br>';

    modalInf += '                                   <div class="form-group row">';

    modalInf += '                                       <label class="form-label col-sm-2 col-form-label text-left text-sm-right" for="operacion">Proveedor:</label>';
    
    modalInf += '                                       <div class="col-lg-4">';
    modalInf += '                                           <select class="form-control" id="cmbProveedor" name="cmbProveedor" disabled>';
    modalInf += '                                           </select>';
    modalInf += '                                       </div>';

    modalInf += '                                   </div>';
    
    modalInf += '                                   </br>';


    modalInf += '                                   <div class="form-group row">';

    modalInf += '                                       <label class="form-label col-sm-2 col-form-label text-left text-sm-right" for="operacion">Stock minimo:</label>';
    
    modalInf += '                                       <div class="col-lg-4">';
    modalInf += '                                           <input type="text" class="form-control" id="txtStockmin" name="txtStockmin" disabled>';
    modalInf += '                                       </div>'; 

    modalInf += '                                   </div>';
    
    modalInf += '                                   </br>';

    modalInf += '                                   <div class="form-group row">';

    modalInf += '                                       <label class="form-label col-sm-2 col-form-label text-left text-sm-right" for="operacion">Tipo Producto:</label>';
    
    modalInf += '                                       <div class="col-lg-4">';
    modalInf += '                                           <select class="form-control" id="cmbTipoProd" name="cmbTipoProd" disabled>';
    modalInf += '                                           </select>';
    modalInf += '                                       </div>'; 

    modalInf += '                                   </div>';
    
    modalInf += '                               </form>';
    modalInf += '                             </div>';            
    modalInf += '                         </div>';       

    modalInf += '                         <div class="modal-footer">';
    
    
        modalInf += '                           <button type="button" class="btn btn-danger btn-pills waves-effect waves-themed" id="btnSalir" name="btnSalir" >SALIR</button>';

        modalInf += '                           <button type="button" class="btn btn-success btn-pills waves-effect waves-themed" onclick="fGuardaProducto();" id="btnGuardaProducto" data-dismiss="modal" name="btnGuardarProducto">GUARDAR</button>';    

        modalInf += '                           <button type="button" class="btn btn-warning btn-pills waves-effect waves-themed" onclick="fGuardaProducto();" id="btnModificarProducto" data-dismiss="modal" name="btnModificarProducto">MODIFICAR</button>';   

        modalInf += '                           <button type="button" class="btn btn-danger btn-pills waves-effect waves-themed" style="background-color:red !important;" onclick="fBorraProducto();" id="btnBorraProducto" data-dismiss="modal" name="btnBorraProducto">ELIMINAR</button>';    

        

    modalInf += '                 </div>';   

    $("#modalProducto").html(modalInf);
}

function fIniTableProducto(){
    $("#tbProducto").dataTable({
        processing: true,
        responsive: true,
        pageLength: 10, 
        altEditor: true,
        select: 'single',
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }],
        language: {
            "emptyTable": "No hay informacion disponible."
        },
        dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        
        buttons: [
            {
                text: '<i class="fal fa-edit mr-1"></i> Agregar',
                className: 'btn-success btn-sm mr-1',
                action: function ( e, dt, node, config ) {
                    let tabla = $('#tbProducto').DataTable().row({ selected: true }).data();
                    
                    $("#txtIdProducto").val(0);
                    
                    $('#txtDescripcion').prop("disabled",false);
                    $('#txtUnimed').prop("disabled",false);
                    $('#cmbProveedor').prop("disabled",false);
                    $('#txtStockmin').prop("disabled",false);
                    $('#cmbTipoProd').prop("disabled",false);

                    $("#btnGuardarProducto").prop("hidden",false);
                    $("#btnModificarProducto").prop("hidden",true);
                    $("#btnBorraProducto").prop("hidden",true);
                    $("#btnSalir").prop("hidden",false).click(function(){
                        $("#modalProducto1").modal("toggle");
                    }); 

                    $("#modalProducto1").modal('show');
                   
                }
            }/*
            {
                extend: 'selected',
                text: '<i class="fal fa-edit mr-1"></i> Editar',
                className: 'btn-warning btn-sm mr-1',
                action: function ( e, dt, node, config ) {
                    
                    let tabla = $('#tbProducto').DataTable().row({ selected: true }).data();
                    
                     $("#txtIdProducto").val(tabla[0]);

                     $('#txtDescripcion').prop("disabled",false);
                     $('#txtUnimed').prop("disabled",false);
                     $('#txtProveedor').prop("disabled",false);
                     $('#txtStockmin').prop("disabled",false);
                     $('#txtTipoProd').prop("disabled",false);


                    
                    $('#txtDescripcion').prop("disabled",false);
                    $('#txtUnimed').prop("disabled",false);
                    $('#txtProveedor').prop("disabled",false);
                    $('#txtStockmin').prop("disabled",false);
                    $('#txtTipoProd').prop("disabled",false);

                    $("#btnGuardarProducto").prop("hidden",false);
                    $("#btnModificarProducto").prop("hidden",true);
                    $("#btnBorraProducto").prop("hidden",true);
                    $("#btnSalir").prop("hidden",false).click(function(){
                        $("#modalProducto1").modal("toggle");
                    }); 

                    $("#modalProducto1").modal('show');

                    $("#txtIdProducto").val(tabla[0]);
                    $('#cmbTipoCosto option:contains(' + tabla[1] + ')').prop('selected', 'selected');
                    $('#cmbConcepto option:contains(' + tabla[2] + ')').prop('selected', 'selected');
                    $("#cmbProveedor option:selected").text(tabla[4]);
                    $("#cmbBuscaFolio option:selected").text(tabla[5]);
                    $("#txtFechaEmision").val(tabla[6]);
                    $("#txtMonto").val(tabla[7]);
                    $("#txtTipoCambio").val(tabla[8]);
                    $("#txtMontoUSD").val(tabla[9]);
                    $("#txtNotaCobro").val(tabla[10]);
                    $("#txtObservacionCosto").val(tabla[11]);
                    $("#txtCuentaVariable").val(tabla[12]);                    
                    $("#txtIdfp").val(tabla[0]);

                    $('#cmbProveedor').prop("disabled",true);
                    $('#cmbBuscaFolio').prop("disabled",true);
                    $('#txtFechaEmision').prop("disabled",true);
                    $('#txtMonto').prop("disabled",true);
                    $('#txtTipoCambio').prop("disabled",true);
                    $('#txtMontoUSD').prop("disabled",true);
                    $('#cmbConcepto').prop("disabled",false);
                    $('#cmbTipoCosto').prop("disabled",false);
                    $('#txtCuentaVariable').prop("disabled",true);
                    $('#txtNotaCobro').prop("disabled",false);
                    $('#txtObservacionCosto').prop("disabled",false);
                    
                    $("#btnGuardarProducto").prop("hidden",true);
                    $("#btnModificarProducto").prop("hidden",false);
                    $("#btnEliminarProducto").prop("hidden",true);

                    $("#modalProducto1").modal('show');
                    

                    
                }
            },
            {   extend: 'selected',
                text: '<i class="fal fa-edit mr-1"></i> Eliminar',
                className: 'btn-danger btn-sm mr-1',
                
                action: function ( e, dt, node, config ) {
                    let tabla = $('#tbProducto').DataTable().row({ selected: true }).data();
                    
                    $("#txtIdProducto").val(tabla[0]);
                    $('#cmbTipoCosto option:contains(' + tabla[1] + ')').prop('selected', 'selected');
                    $('#cmbConcepto option:contains(' + tabla[2] + ')').prop('selected', 'selected');
                    $("#cmbProveedor option:selected").text(tabla[4]);
                    $("#cmbBuscaFolio option:selected").text(tabla[5]);
                    $("#txtFechaEmision").val(tabla[6]);
                    $("#txtMonto").val(tabla[7]);
                    $("#txtTipoCambio").val(tabla[8]);
                    $("#txtMontoUSD").val(tabla[9]);
                    $("#txtNotaCobro").val(tabla[10]);
                    $("#txtObservacionCosto").val(tabla[11]);
                    $("#txtCuentaVariable").val(tabla[12]);

                    $('#cmbProveedor').prop("disabled",true);
                    $('#cmbBuscaFolio').prop("disabled",true);
                    $('#txtFechaEmision').prop("disabled",true);
                    $('#txtMonto').prop("disabled",true);
                    $('#txtTipoCambio').prop("disabled",true);
                    $('#txtMontoUSD').prop("disabled",true);
                    $('#cmbConcepto').prop("disabled",true);
                    $('#cmbTipoCosto').prop("disabled",true);
                    $('#txtCuentaVariable').prop("disabled",true);
                    $('#txtNotaCobro').prop("disabled",true);
                    $('#txtObservacionCosto').prop("disabled",true);

                    $("#btnGuardarProducto").prop("hidden",true);
                    $("#btnModificarProducto").prop("hidden",true);
                    $("#btnEliminarProducto").prop("hidden",false);

                    $("#modalProducto1").modal('show');
                    
                } */
            /* } */
        ],
        onAddRow: function(dt, rowdata, success, error){ 
        },        
        onEditRow: function(dt, rowdata, success, error){
        },
        onDeleteRow: function(dt, rowdata, success, error){
        }
    });
}

function fDibujaTablaProducto(datos){
    
    let tableInfo = '';
    tableInfo += '<thead class="bg-highlight">';
    tableInfo += '    <tr>';
    tableInfo += '        <th>ID</th>';
    tableInfo += '        <th>Descripcion</th> ';
    tableInfo += '        <th>Unidad de medida</th> ';
    tableInfo += '        <th>Proveedor</th> ';
    tableInfo += '        <th>Stock minimo</th> ';
    tableInfo += '        <th>Tipo Producto</th> ';
    tableInfo += '    </tr>';
    tableInfo += '</thead>';
    tableInfo += '<tbody>';

    datos.forEach(function(valor){
            
        tableInfo +='    <tr>';
        tableInfo +='        <td>'+valor.id+'</td>';
        tableInfo +='        <td>'+valor.descripcion+'</td>';
        tableInfo +='        <td>'+valor.uni_med+'</td>';
        tableInfo +='        <td>'+valor.proveedor+'</td>';
        tableInfo +='        <td>'+valor.stock_min+'</td>';
        tableInfo +='        <td>'+valor.tipo_prod+'</td>';
        tableInfo +='    </tr>';
    
    });

    tableInfo += '</tbody>';
    tableInfo += '<tfoot class="thead-themed">';
    tableInfo += '    <tr>';
    tableInfo += '        <th>ID</th>';
    tableInfo += '        <th>Descripcion</th> ';
    tableInfo += '        <th>Unidad de medida</th> ';
    tableInfo += '        <th>Proveedor</th> ';
    tableInfo += '        <th>Stock minimo</th> ';
    tableInfo += '        <th>Tipo Producto</th> ';
    tableInfo += '    </tr>';
    tableInfo += '</tfoot>';
                                                                    


$("#tbProducto").html(tableInfo);
$("#tbProducto").DataTable().destroy();
fIniTableProducto();
}


function fBuscaProducto(){
    datos = {"busqueda" : "buscaProducto"};

    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,async      :   false
        ,beforeSend : function(){
                        //fmuestraCargando();
                        }
        ,success    :   function(resultado){
                        console.log("aklala", resultado.data);
                        fDibujaTablaProducto(resultado.data);
                        }
        ,error:function(response){ 
 check_response_error(response);
                         console.log('error-->'+response);
                         fgReemplazaModalResp("Error. 4J4X-5C41PT01",1);
                            //focultaCargando();
                        }
    });

}

function fBorraProducto(rowdata){
    let datos    = {"busqueda" : "borraProducto", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbProducto').DataTable().destroy();   
                                fBuscaProducto();                                                               
                            }else{
                                console.log(resultado.data.resultado);
                                //fgReemplazaModalResp(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}

function fGuardaProducto(rowdata){
    let datos    = {"busqueda" : "guardaProducto", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbProducto').DataTable().destroy();   
                                fBuscaProducto();                                                               
                            }else{
                                console.log(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}

/* `````````````````````````````````````````````````````````````````````````````````` */
function fBuscaTipoProducto(){
    let datos   = {"busqueda" : "buscaTipoProducto"};
    
   

    var columnSet = [{
        title: "ID",
        id: "id_tp",
        data: "id",  
        type: "readonly",
        defaultValue: '0'
    },
    {
        title: "Rut",
        id: "descripcion_tp",
        data: "descripcion",
        type: "text",
    }
  ]


    $('#tbTipoProd').dataTable({
        /* check datatable buttons page for more info on how this DOM structure works */
        dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        //ajax: "media/data/server-demo.json",
        ajax :{
            url         :   'select.php'
            ,data       :   datos
            ,type       :   "POST"
            ,dataType   :   "json"
        },
        columns: columnSet,
        /* selecting multiple rows will not work */
        select: 'single',
        /* altEditor at work */
        altEditor: true,
        responsive: true,
        language: {
            "emptyTable": "No hay datos disponibles."
        },
        pageLength: 50, 
        /* buttons uses classes from bootstrap, see buttons page for more details */
        buttons: [
        
        {
            text: '<i class="fal fa-plus mr-1"></i> Agregar',
            name: 'add',
            className: 'btn-success btn-sm mr-1'
        },
        {
            extend: 'selected',
            text: '<i class="fal fa-edit mr-1"></i> Editar',
            name: 'edit',
            className: 'btn-primary btn-sm mr-1'
        },
        {
            extend: 'selected',
            text: '<i class="fal fa-trash mr-1"></i> Eliminar',
            name: 'delete',
            className: 'btn-danger btn-sm mr-1'
        }
        ]
       
        ,onAddRow: function(dt, rowdata, success, error){ 
            fGuardaTipoProducto(rowdata)
            
        },
        onEditRow: function(dt, rowdata, success, error){ 
            fGuardaTipoProducto(rowdata)
        },
        onDeleteRow: function(dt, rowdata, success, error){
            success(rowdata);
            fBorraTipoProducto(rowdata);
        }
    });      
}

function fBorraTipoProducto(rowdata){
    let datos    = {"busqueda" : "borraTipoProducto", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbTipoProd').DataTable().destroy();   
                                fBuscaTipoProducto();                                                               
                            }else{
                                console.log(resultado.data.resultado);
                                //fgReemplazaModalResp(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}

function fGuardaTipoProducto(rowdata){
    let datos    = {"busqueda" : "guardaTipoProducto", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbTipoProd').DataTable().destroy();   
                                fBuscaTipoProducto();                                                               
                            }else{
                                console.log(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}