$(document).ready(function(){

    $("#btnBuscar").click(function() {
        fbuscaTransacciones();
    });    

     $("#cmbEspecieComex").change(function(){
        fCreaNuevaOperacion();
    });

fLlenaSesion();
   
$("#btnNueva").click(function() {
    if(sessionStorage.getItem("tipoUser") == "1"){
        fgReemplazaModalResp("Usted no tiene permiso para poder realizar una nueva Operacion FS<br>Esta en modo visualizacion",0);
    }else{        
    window.location.href = "DetalleDocumento.php?idDocumento=0";
    }
}); 

 
});

function fLlenaSesion(){
 
    let datostipodocumento = {'busqueda': 'buscaCMB', 'cmb': 'cmbTipoDocumento', 'sp': 'CALL SPWEB_Search_CMB_TipoDocumento()'}
    ,datoscliente = {'busqueda': 'buscaCMB', 'cmb': 'cmbCliente', 'sp': 'CALL SPWEB_Search_Cmb_Cliente()'};
    //fListaOperaciones();
    fIniTable(); 
    

    //fDibujaModal2();
    fListasCmb(datostipodocumento,'--TODOS--');
    fListasCmb(datoscliente,'--TODOS--');

    //fListaOperaciones();

    //sessionStorage.clear();
    
    sessionStorage.setItem("controlMonto",0);
    
}


function fIniTable(){
    $("#tbTransacciones").dataTable({
        responsive: true,
        pageLength: 30,
        /* order: [
            [1, 'desc']
        ], */
        
        /*rowGroup:
        {
            dataSrc: 0
        },*/
        language: {
            "emptyTable": "No existen transacciones disponibles."
        }
    });
}
function fLimpiaTable(){
    console.log("destroyed tbl");
    let tableInfo = '';
    tableInfo +='<thead class="bg-highlight">';
    tableInfo +='    <tr>';
    tableInfo +='         <th>ID</th>';
    tableInfo +='         <th>TIPO DOCUMENTO</th>';
    tableInfo +='         <th>CLIENTE/PROVEEDOR</th>';
    tableInfo +='         <th>FECHA</th>';
    tableInfo +='         <th>NETO</th>';
    tableInfo +='         <th>IVA</th>';
    tableInfo +='         <th>TOTAL</th>';
    tableInfo +='         <th>ESTADO</th>';
    tableInfo +='         <th>ACCIÓN</th>';
    tableInfo +='    </tr>';
    tableInfo +='</thead>';
    tableInfo +='<tbody>';
    tableInfo +='</tbody>';
    tableInfo +='<tfoot class="thead-themed" >';
    tableInfo +='    <tr>';
    tableInfo +='       <th>ID</th>';
    tableInfo +='       <th>TIPO DOCUMENTO</th>';
    tableInfo +='       <th>CLIENTE/PROVEEDOR</th>';
    tableInfo +='       <th>FECHA</th>';
    tableInfo +='       <th>NETO</th>';
    tableInfo +='       <th>IVA</th>';
    tableInfo +='       <th>TOTAL</th>';
    tableInfo +='       <th>ESTADO</th>';
    tableInfo +='       <th>ACCIÓN</th>';
    tableInfo +='    </tr>';
    tableInfo +='</tfoot>';

    $("#tbTransacciones").html(tableInfo);
    fIniTable(); 
}
function fListasCmb(datos,primerOption,onchange){
    //let datos = {"busqueda" : "buscaMaterial","cmb" : "cmbMaterial"};     
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,async      :   false
        ,beforeSend : function(){
                        //fmuestraCargando();
                        }
        ,success    :   function(resultado){
                            
                            if (resultado.success){
                                fgLlenaCmb(resultado,"onchange",onchange,primerOption,"",false);
                                
                            }else{
                                fgReemplazaModalResp(resultado.data.mensaje,1);
                            }
                            
                            //$("#cmbShipper").val(0);
                            
                            //focultaCargando();
                        }
        ,error:function(response){ 

                         console.log('error-->'+response);
            
                            //focultaCargando();
                        }
    });

    
}
function fgLlenaCmb(arreglo,accion,funct,inic,def,disabl){	
	var cmb = $('#'+arreglo["data"]["cmb"]); //obtengo objeto
	cmb.find('option').remove();//limpiar cmb
	var cantidad 	=	arreglo['data']['listado'].length	//23-10-2019
		,codigo		=	-1;
	//Validar si se encontraron datos para combobox
	if (arreglo['data']['listado'].length > 0){
		if (inic != ''){	//se agrega valor inicial
			cmb.append('<option value="0">'+inic+'</option>');
		}
		$(arreglo['data']['listado']).each(function(i,v){	//llena cmb
			codigo	=	v.Codigo;//23-10-2019
			cmb.append('<option value="'+v.Codigo+'">'+v.Descripcion+'</option>');			
		});

		//cmb.trigger("change");
		//cmb.change();

		if (accion != ''){
			cmb.attr(accion,funct);	//asigno accion / funcion
		}
		if (def!=''){	//asigna valor por defecto
			cmb.val(def);	
		}else if (cantidad == 1){	//23-10-2019
			cmb.val(codigo);
			cmb.change();
		}
		// if (disabl != disabl){ //bloquea cmb
		// 	cmb.attr(disabl,disabl);	
		// }
		cmb.prop("disabled",disabl);	
	}else{
		cmb.append('<option value="-1">No se encontro informacion</option>');
	}
}
function fbuscaTransacciones(){
    let msje = '';
    let cliente = $("#cmbCliente").val()
    ,tipoDocumento = $("#cmbTipoDocumento").val()
    ,fechaDesde = $("#txtFechaDesde").val()
    ,fechaHasta = $("#txtFechaHasta").val();


    let datos      = {"busqueda" : "buscaTransacciones" 
                    , "cliente" : cliente
                    , "tipoDocumento" : tipoDocumento
                    , "fechaDesde" : fechaDesde 
                    , "fechaHasta" : fechaHasta};

    if(msje.length == 0){
        $.ajax({
            url         :   'select.php'
            ,data       :   datos
            ,type       :   "POST"
            ,dataType   :   "json"
            ,beforeSend : function(){
                            //fmuestraCargando();
                            }
            ,success    :   function(resultado){
                                if (resultado.success){
                                    $('#tbTransacciones').DataTable().destroy();
                                    fLimpiaTable;
                                    fDibujaTablaTransacciones(resultado);
                                    
                                }else{
                                    console.log(resultado.data.mensaje,1);
                                }
                                
                                //focultaCargando();
                            }
            ,error:function(response){ 
                                console.log(response);
                                //focultaCargando();
                            }
        });
    }else{
        fgReemplazaModalResp(msje,1);
    }
}

function fDibujaTablaTransacciones (datos){
    console.log(datos);
    let tableInfo = '';
    tableInfo +='<thead class="bg-highlight">';
    tableInfo +='    <tr>';
    tableInfo +='         <th>ID</th>';
    tableInfo +='         <th>TIPO DOCUMENTO</th>';
    tableInfo +='         <th>CLIENTE/PROVEEDOR</th>';
    tableInfo +='         <th>FECHA</th>';
    tableInfo +='         <th>NETO</th>';
    tableInfo +='         <th>IVA</th>';
    tableInfo +='         <th>TOTAL</th>';
    tableInfo +='         <th>ESTADO</th>';
    tableInfo +='         <th>ACCIÓN</th>';
    tableInfo +='    </tr>';
    tableInfo +='</thead>';
    tableInfo +='<tbody>';
    datos.data.listado.forEach(function(valorZ,indiceZ,arrayZ){  
        tableInfo +='    <tr>';
        
        tableInfo +='        <td>'+valorZ.id+'</td>';
        tableInfo +='        <td>'+valorZ.td+'</td>';      
        tableInfo +='        <td>'+valorZ.cliente+'</td>';
        tableInfo +='        <td>'+valorZ.fecha+'</td>';  
        tableInfo +='        <td>'+valorZ.neto+'</td>';   
        tableInfo +='        <td>'+valorZ.iva +'</td>';   
        tableInfo +='        <td>'+valorZ.total+'</td>';    
        tableInfo +='        <td>'+valorZ.estado+'</td>';    
        tableInfo +='        <td align = "center">'; 
        //tableInfo +='        <td>'+valorZ.estado+'</td>';    
        /* tableInfo +='        <button type = "button" class="btn btn-sm btn-outline-success btn-icon btn-inline-block mr-2" id = "btnCopy'+indiceZ+'" name = "btnCopy'+indiceZ+'" onclick = "fcargaOperaciones();"><i class="fal fa-clone"></i></button>';    */    
        if (valorZ.estado == "VIGENTE"){
            tableInfo +='<a href="DetalleDocumento.php?idDocumento='+valorZ.id+'" class="btn btn-sm btn-outline-primary btn-icon btn-inline-block mr-2"><i class="bi-eye"></i></a>';
            tableInfo +='<button type = "button" class="btn btn-sm btn-outline-danger btn-icon btn-inline-block mr-2" id = "btnDelete'+valorZ.id+'" name = "btnDelete'+valorZ.id+'" onclick = "fDeleteOperacion('+valorZ.id+');"><i class="bi-eraser"></i></button>';   
        }else{
            tableInfo +='<a href="DetalleDocumento.php?idDocumento='+valorZ.id+'" class="btn btn-sm btn-outline-warning btn-icon btn-inline-block mr-2"><i class="x-square-fill"></i></a>';
            tableInfo +='<button type = "button" class="btn btn-sm btn-outline-danger btn-icon btn-inline-block mr-2" id = "btnDelete'+valorZ.id+'" name = "btnDelete'+valorZ.id+'" onclick = "fDeleteOperacion('+valorZ.id+');"><i class="bi-check"></i></button>';   

        }
        tableInfo +='</td>';   
        tableInfo +='</tr>';

    });

    tableInfo +='</tbody>';
    tableInfo +='<tfoot class="thead-themed" >';
    tableInfo +='    <tr>';
    tableInfo +='       <th>ID</th>';
    tableInfo +='       <th>TIPO DOCUMENTO</th>';
    tableInfo +='       <th>CLIENTE/PROVEEDOR</th>';
    tableInfo +='       <th>FECHA</th>';
    tableInfo +='       <th>NETO</th>';
    tableInfo +='       <th>IVA</th>';
    tableInfo +='       <th>TOTAL</th>';
    tableInfo +='       <th>ESTADO</th>';
    tableInfo +='       <th>ACCIÓN</th>';
    tableInfo +='    </tr>';
    tableInfo +='</tfoot>';

    $("#tbTransacciones").html(tableInfo);
    fIniTable();
}