$(document).ready(function(){
    /* $('#tbproveedor').DataTable(); */
    fbuscaProveedores();
});

function fbuscaProveedores(){
    let datos   = {"busqueda" : "buscaProveedor"};
    
   

    var columnSet = [{
        title: "ID",
        id: "id_proveedor",
        data: "id",  
        type: "readonly",
        defaultValue: '0'
    },
    {
        title: "Nombre",
        id: "nombre_proveedor",
        data: "nombre",
        type: "text"
    },
    {
        title: "Direccion",
        id: "direccion_proveedor",
        data: "direccion",
        type: "text"
    },
    {
        title: "Ciudad",
        id: "ciudad_proveedor",
        data: "ciudad",
        type: "text"
    },
    {
        title: "Telefono",
        id: "fono_proveedor",
        data: "fono",
        type: "text"
    },
    {
        title: "Contacto",
        id: "contacto_proveedor",
        data: "contacto",
        type: "text"
    }    
  ]


    $('#tbProveedor').dataTable({
        /* check datatable buttons page for more info on how this DOM structure works */
        dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        //ajax: "media/data/server-demo.json",
        ajax :{
            url         :   'select.php'
            ,data       :   datos
            ,type       :   "POST"
            ,dataType   :   "json"
        },
        columns: columnSet,
        /* selecting multiple rows will not work */
        select: 'single',
        /* altEditor at work */
        altEditor: true,
        responsive: true,
        language: {
            "emptyTable": "No hay datos disponibles."
        },
        pageLength: 50, 
        /* buttons uses classes from bootstrap, see buttons page for more details */
        buttons: [
        
        {
            text: '<i class="fal fa-plus mr-1"></i> Agregar',
            name: 'add',
            className: 'btn-success btn-sm mr-1'
        },
        {
            extend: 'selected',
            text: '<i class="fal fa-edit mr-1"></i> Editar',
            name: 'edit',
            className: 'btn-primary btn-sm mr-1'
        },
        {
            extend: 'selected',
            text: '<i class="fal fa-trash mr-1"></i> Eliminar',
            name: 'delete',
            className: 'btn-danger btn-sm mr-1'
        }
        ]
       
        ,onAddRow: function(dt, rowdata, success, error){ 
            fGuardaProveedor(rowdata)
            
        },
        onEditRow: function(dt, rowdata, success, error){ 
            fGuardaProveedor(rowdata)
        },
        onDeleteRow: function(dt, rowdata, success, error){
            
            
            success(rowdata);
            fBorraProveedor(rowdata);
        }
    });      
}


function fBorraproveedor(rowdata){
    let datos    = {"busqueda" : "borraProveedor", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbProveedor').DataTable().destroy();   
                                fbuscaProveedores();                                                               
                            }else{
                                console.log(resultado.data.resultado);
                                //fgReemplazaModalResp(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}

function fGuardaProveedor(rowdata){
    let datos    = {"busqueda" : "guardaProveedor", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbProveedor').DataTable().destroy();   
                                fbuscaProveedores();                                                               
                            }else{
                                console.log(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}