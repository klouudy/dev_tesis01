$(document).ready(function(){
    /* $('#tbCliente').DataTable(); */
    fbuscaClientes();
});

function fbuscaClientes(){
    let datos   = {"busqueda" : "buscaCliente"};
    
   

    var columnSet = [{
        title: "ID",
        id: "id_cliente",
        data: "id",  
        type: "readonly",
        defaultValue: '0'
    },
    {
        title: "Rut",
        id: "rut_cliente",
        data: "rut",
        type: "text",
    },
    {
        title: "DV",
        id: "dv_cliente",
        data: "dv",
        type: "text",
    },
    {
        title: "Nombre",
        id: "nombre_cliente",
        data: "nombre",
        type: "text"
    },
    {
        title: "Direccion",
        id: "direccion_cliente",
        data: "direccion",
        type: "text"
    },
    {
        title: "Ciudad",
        id: "ciudad_cliente",
        data: "ciudad",
        type: "text"
    },
    {
        title: "Telefono",
        id: "fono_cliente",
        data: "fono",
        type: "text"
    },
    {
        title: "Contacto",
        id: "contacto_cliente",
        data: "contacto",
        type: "text"
    }    
  ]


    $('#tbCliente').dataTable({
        /* check datatable buttons page for more info on how this DOM structure works */
        dom: "<'row mb-2'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        //ajax: "media/data/server-demo.json",
        ajax :{
            url         :   'select.php'
            ,data       :   datos
            ,type       :   "POST"
            ,dataType   :   "json"
        },
        columns: columnSet,
        /* selecting multiple rows will not work */
        select: 'single',
        /* altEditor at work */
        altEditor: true,
        responsive: true,
        language: {
            "emptyTable": "No hay datos disponibles."
        },
        pageLength: 10, 
        /* buttons uses classes from bootstrap, see buttons page for more details */
        buttons: [
        {
            text: '<i class="fal fa-plus mr-1"></i> Agregar',
            name: 'add',
            className: 'btn-success btn-sm mr-1'
        },
        {
            extend: 'selected',
            text: '<i class="fal fa-edit mr-1"></i> Editar',
            name: 'edit',
            className: 'btn-primary btn-sm mr-1'
        },
        {
            extend: 'selected',
            text: '<i class="fal fa-trash mr-1"></i> Eliminar',
            name: 'delete',
            className: 'btn-danger btn-sm mr-1'
        }
        ]
       
        ,onAddRow: function(dt, rowdata, success, error){ 
            success(rowdata);
            fGuardaCliente(rowdata)
            /* let msg = ""
            
            if(rowdata.nombre.length == 0){
                msg += "*Se debe ingresar un Nombre. <br>"
            }
            if(rowdata.rut.length == 0){
                msg += "*Se debe ingresar Rut. <br>"
            }
            if(rowdata.dv.length == 0){
                msg += "*Se debe ingresar Digito Verificador. <br>"
            }
            if(msg.length > 0){*/
                /* $(".btn-default").click(); */
            /*    console.log(msg);
            }else{
                success(rowdata);
                
            } */
            
        },
        onEditRow: function(dt, rowdata, success, error){ 
            success(rowdata);
            fGuardaCliente(rowdata)
            /* let msg = ""
            if(rowdata.nombre.length == 0){
                msg += "*Debes ingresar Descripcion. <br>"
            }
            if(rowdata.rut.length == 0){
                msg += "*Debes ingresar Rut. <br>"
            }
            if(rowdata.dv.length == 0){
                msg += "*Debes ingresar DV. <br>"
            }
            if(msg.length > 0){
                $(".btn-default").click();
                fgReemplazaModalResp(msg,1);
            }else{
                success(rowdata);
                fGuardaCliente(rowdata)
            } */
        },
        onDeleteRow: function(dt, rowdata, success, error){
            
            
            success(rowdata);
            fBorraCliente(rowdata);
        }
    });      
}


function fBorraCliente(rowdata){
    let datos    = {"busqueda" : "borraCliente", "data" : rowdata};
    $.ajax({
        url         :   'select.php'
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbCliente').DataTable().destroy();   
                                fbuscaClientes();                                                               
                            }else{
                                console.log(resultado.data.resultado);
                                //fgReemplazaModalResp(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}

function fGuardaCliente(rowdata){
    let datos    = {"busqueda" : "guardaCliente", "data" : rowdata};
    $.ajax({
        url         :   "select.php"
        ,data       :   datos
        ,type       :   "POST"
        ,dataType   :   "json"
        ,beforeSend : function(){
        console.log(datos);
            
                        }
        ,success    :   function(resultado){
                            if (resultado.success){
                                console.log(resultado.data.respuesta,resultado.data.resultado);  
                                $('#tbCliente').DataTable().destroy();   
                                fbuscaClientes();                                                               
                            }else{
                                console.log(resultado.data.mensaje,1);
                            }
                        }
        ,error      :   function(response){
                            console.log(response);
                            console.log("Error. 4J4X-5C41PT15",1);
                        }
    });
}